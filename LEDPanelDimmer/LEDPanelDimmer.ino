/*
TABS=3
LED Panel Dimmer project -- D. M. Lenardon 2016-04-15
using PWM with an N-channel MOSFET for switching and an NPN to control the current
Using ESP8266 because it can do 12-bit PWM and WiFI
PWM_out_pin is connected thru R1 to MOSFET gate and NPN collector
up_button_pin is for an Up Button momentary contact switch
down_button_pin is for a Down Button momentary contact switch
TST_FLSH is for a battery test (and ESP8266 flash) Button momentary contact switch

==== IMPORTANT ====
The current release (2.3.0) ESP8266 core libraries have a bug in the PWM code that results in severe LED flicker and other anomolies.
The PWM fix is not yet in an ESP8266 core release.
If you are building this before the fix for #836 is in a release,
then you need to use the live GitHub repo. If you don't, then you will see some occasional
bright flashes at low light levels as well as noticeable flickering.

See https://www.hackster.io/28377/led-light-panel-rebuild-super-dimming-and-remote-control-5478b7?team=28377
for complete project documentation.

Copyright 2016 D. M. Lenardon
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
*/

// these are here mostly to make VisualStudio happy - Intellisense gets confused if these are only declared in  the .cpp file
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>
#include <WiFiServer.h>
#include <WiFiClientSecure.h>
#include <WiFiClient.h>
#include <ESP8266WiFiType.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFi.h>
#include "common.h"

//#define DEBUG
//#define DEVBOARD

#include <EEPROM.h>
#include <ArduinoOTA.h>
#include <LEDManager.h>                     //  download from https://github.com/Rom3oDelta7/LEDManager

RGBLED	led(PWR_R, PWR_G, PWR_B);
bool     LEDSetupOn = true;

// Global variables used in interrupt routines or WiFi service routine
class ButtonState {                                      // manage state for up and down buttons
public:
   bool 		debounce = false;
   unsigned long debounce_start = 0; 
   static   bool doublePress;                            // static -> common state flag for up & down buttons
};

volatile uint8_t	     PWM_index = 0;							// WiFI modifable
volatile unsigned long current_time = 0;
volatile ButtonState   ButtonUp, ButtonDown;
volatile bool          configAborted = false;

bool ButtonState::doublePress = false;                   // static class member initialization

// for export to WiFI service
float		stops;
float    battery_volts;
LEDColor battery_color = LEDColor::NONE;

// from WiFi service 
extern bool panelEnable;
extern bool LEDTestSave;
extern void setupWiFi(void);
extern void WiFiService(void);
extern void STAMode(void);
extern bool OTAenabled;
extern bool WiFiConfigInProgress;
extern WiFiManager wifiManager;

// ISR handler for up button
ICACHE_RAM_ATTR void upButtonISR (void) {  // esp8266 core 2.5.1+ now check and abort if at least top level ISRs are not in IRAM

   os_intr_lock();                // disable interrupts
   if (!ButtonUp.debounce) {
      // increment the index and let the main loop implement the change
      PWM_index = constrain(PWM_index + 1, 0, PWM_index_max - 1);
      ButtonUp.debounce = true;
      ButtonUp.debounce_start = current_time;
   }
   if ( ButtonUp.debounce && ButtonDown.debounce && !ButtonDown.doublePress) {
      /*
       Both buttons pressed simultaneously
       A double press of the up/down buttons has two functions:
         1. abort config portal while it is in progress
         2. disable connection status (and associated SSID broadcast) after config portal is complete

       Note that 2nd press will undo the index change from the first button
       Also, when this is called during WiFi setup, there is no function to reset the debounce flag, so
        you can achieve the same result by pressing one button and then the other
      */
      ButtonDown.doublePress = true;                              // static class member so this is a shared variable
      if ( WiFiConfigInProgress ) {
         wifiManager.abortConfigPortal();
         configAborted = true;
      }
   }
   os_intr_unlock();              // re-enable interrupts
}

// ISR handler for down button
ICACHE_RAM_ATTR void downButtonISR (void) {  // esp8266 core 2.5.1+ now check and abort if at least top level ISRs are not in IRAM
 
   os_intr_lock();                // disable interrupts
   if (!ButtonDown.debounce) {
      // decrement the index and let the main loop implement the change
      PWM_index = constrain(PWM_index - 1, 0, PWM_index_max - 1);
      ButtonDown.debounce = true;
      ButtonDown.debounce_start = current_time;
   }
   if ( ButtonDown.debounce && ButtonUp.debounce && !ButtonUp.doublePress) {
      ButtonUp.doublePress = true;
      if ( WiFiConfigInProgress ) {
         wifiManager.abortConfigPortal();
         configAborted = true;
      }
   }
   os_intr_unlock();              // re-enable interrupts
}

void setup ( void ) {
   Serial.begin(74880);
   delay(1000);
#if defined(DEBUG)
   Serial.println("Startup ESP8266");
#endif

   delay(200);										// workaround for strange startup behavior
   pinMode(PWM_out_pin, OUTPUT);
   digitalWrite(PWM_out_pin, 0);
   pinMode(up_button_pin, INPUT_PULLUP); 
   pinMode(down_button_pin, INPUT_PULLUP);
   pinMode(TST_FLSH, INPUT_PULLUP);

   attachInterrupt(digitalPinToInterrupt(up_button_pin), upButtonISR, FALLING);
   attachInterrupt(digitalPinToInterrupt(down_button_pin), downButtonISR, FALLING);

   analogWriteFreq(66);
   analogWriteRange(4095);
   led.setState(LEDState::OFF);
   EEPROM.begin(EEPROMSIZE);
   PWM_index = EEPROM.read(EEPROM_PWM_index_addr);
#if defined(DEBUG)
   Serial.print("Stored PWM value was: "); Serial.println(PWM_index);
#endif

   if ( PWM_index > (PWM_index_max - 1) ) {
      // deal with initial garbage in EEPROM
      PWM_index = 0;
   }

#if defined(DEBUG)
   Serial.print("Initial PWM value: "); Serial.println(PWM_index);
#endif

   setupWiFi();
   ButtonUp.debounce = true;                    // start disabled for loop()
   ButtonDown.debounce = true;
}

// save current brightness (PWM_index) to EEPROM for next session
void savePWMIndex ( void ) {
   static int lastIndex = -1;
   
   if ( lastIndex != (int)PWM_index ) {
      lastIndex = (int)PWM_index;
#if defined(DEBUG)
      Serial.println("Saving new index: " + String(PWM_index));
#endif
      EEPROM.write(EEPROM_PWM_index_addr, PWM_index);
      EEPROM.commit();
   }
}

/*
 service routine to process buttons, battery, and other hardware
*/
void panelService ( void ) {

   const uint16_t  PWM_duty_cycle [] = {0, 1, 2, 3, 4, 5, 7, 10, 14, 21, 29, 42, 59, 84, 119, 170, 241, 340, 484, 687, 949, 1387, 1987, 2842, 4095};
   static uint8_t  prior_PWM_index = 0;
   uint16_t 		 sample[3] = {0,0,0};
   static unsigned long battery_check_time = 0;
   uint16_t	       battery_level;


   current_time = millis();

   if ( PWM_index !=  prior_PWM_index ) {  						// If switch interrupts have changed the index
      prior_PWM_index = PWM_index;
      stops = float((PWM_index_max -1)-PWM_index) / -2.0;
#if defined(DEBUG)
      Serial.print("Index changed to "); Serial.println(PWM_index);
      Serial.print("New dimming value is: "); Serial.print(stops); Serial.println(" stops");
#endif
   }
   if ( panelEnable ) {
      analogWrite(PWM_out_pin, PWM_duty_cycle[PWM_index]);
   } else {
      analogWrite(PWM_out_pin, 0);
   }
   
   // don't look for other button changes until after debounce time
   if ( ButtonUp.debounce && ((current_time - ButtonUp.debounce_start) > 250) ) {
      ButtonUp.debounce = false;
   }
   if (ButtonDown.debounce && ((current_time - ButtonDown.debounce_start) > 250)) {
      ButtonDown.debounce = false;
   }

   if (ButtonUp.doublePress) {
      // stop broadcastuing the IP address through the SSID, disable status LED
      ButtonUp.doublePress = false;
      LEDSetupOn = false;
      STAMode();                // also enables OTA updates
   }

   if ( (LEDTestSave || (digitalRead(TST_FLSH) == LOW)) && (current_time > battery_check_time) ) {
      battery_check_time = current_time + 1000; // only check the battery once every second
   
      battery_level = 0;
      for (uint8_t i = 0; i < 3; i++) {
         sample[i] = analogRead(A0);
         delay(1);
         if(sample[i] > battery_level) {
            battery_level = sample[i];
         }
      }
      // apply correction factor to get ADC voltage (after divider) closer to actual millivolts
#if defined(DEVBOARD)
      // 3.3V baseline for ESP8266 development boards
      battery_volts = float(battery_level) * 0.009766 * 3.3;			
#else
      battery_volts = float(battery_level) * .009766;	//1024 (1.0V)at ADC = 10V at battery
#endif
      battery_volts = constrain(battery_volts, 0., 10.00);						// 1023 * formula + headroom

#if defined(DEBUG)
      Serial.print("samples:"); Serial.print(sample[0]); Serial.print(sample[1]); Serial.println(sample[2]);
      Serial.print("battery_level: "); Serial.println(battery_level);
#endif
      if(battery_volts > battery_yellow) {
         battery_color = LEDColor::GREEN;
      }
      else if(battery_volts > battery_red) {
         battery_color = LEDColor::ORANGE;
      }
      else {
         battery_color = LEDColor::RED;
      }
   }
   /*
    hold down the button to show value on LED or switch on in WiFI interface
    this also saves the current index in EEPROM

    LED may also be on initially to show setup state
   */
   bool buttonPressed = (digitalRead(TST_FLSH) == LOW);
   if ( buttonPressed ) {
      savePWMIndex();
   }
   // if still displaying setup state colors, leave as-is until turned off
   if (!LEDSetupOn) {
      if (LEDTestSave || buttonPressed) {
         led.setColor(battery_color);
         led.setState(LEDState::ON);
      } else {
         led.setState(LEDState::OFF);
      }
   }
}

// light the LED to indicate setup errors - THIS FUNCTION DOES NOT RETURN
void fatalError ( LEDColor color ) {
   led.setColor(color);
   led.setState(LEDState::BLINK_ON);
   while ( true ) {
      delay(100);             // avoid WDT
   }
}

/*
  main loop
*/
void loop ( void ) {
   
   if ( OTAenabled ) {
      ArduinoOTA.handle();
   }
   panelService();
   if ( !configAborted ) {
      // aborted config => manual mode, no WiFi
      WiFiService();
   }
   yield();            // does nothing if WiFi off but does prevent WDTs
}
