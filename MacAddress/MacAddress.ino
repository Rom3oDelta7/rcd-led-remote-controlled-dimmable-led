#include <ESP8266WiFi.h>
#include <WiFiClient.h> 

void setup ( void ) {
	uint8_t	mac[WL_MAC_ADDR_LENGTH];
	
	delay(1000);
	Serial.begin(115200);

	WiFi.softAPmacAddress(mac);
	Serial.print("MAC address is ");
	for ( uint8_t i = 0; i < WL_MAC_ADDR_LENGTH; i++ ) {
		Serial.print(mac[i], HEX);
		if ( i < (WL_MAC_ADDR_LENGTH-1) ) {
			Serial.print(":");
		}
	}
	Serial.println();

}

void loop ( void ) {}