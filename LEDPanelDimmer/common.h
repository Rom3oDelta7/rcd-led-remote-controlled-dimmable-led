
/*
   TABS=3

   Copyright 2016 Dan Lenardon & Rob Redford
   This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
   To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
   
*/
#ifndef COMMON_H
#define COMMON_H

// pins for ESP8266
#define PWM_out_pin 			4    		   // For 8266 D0 PWM pin
#define up_button_pin 		2   			// For 8266 D4 interrupt
#define down_button_pin		5 				// For 8266 D5 interrupt
#define EEPROMSIZE 			256   		// ESP8266-12F has 4MB Flash but don't need to reserve much
#define CRED_ADDR          128         // start of WiFI saved credentials in EEPROM
#define PWM_index_max 		25    		// size of PWM table for 12-bit PWM
#define PWR_G 					12 			// battery level LED-G pin
#define PWR_B 					14 			// battery level LED-B pin
#define PWR_R 					13 			// battery level LED-R pin
#define TST_FLSH 				0		  		// pin used for both battery test and Flash

/*
    battery voltage thresholds
    ESP8266 ADC pin is connected through a voltage divider 9.1k/1k and reads 0-1023
    ESP8266 ADC sometimes jumps; -49 units with WiFi on, ~-20 w/ WiFI OFF
    correction factor was determined by measurement to get reading close to actual volts
*/
#define battery_yellow 		7.08   		// 7.08 V
#define battery_red 			6.60			// 6.6 V

#define EEPROM_PWM_index_addr 16 		// location of EEPROM-stored index into PWM_duty_cycle table

#endif