/*
   TABS=3

   LED light panel control WiFi browser interface
   
   Prior to running this sketch, the HTML file must be loaded into the ESP8266 SPIFFS file system.
   Currently, the instructions for installing and running the uplaod tool can be found here: http://esp8266.github.io/Arduino/versions/2.3.0/doc/filesystem.html

   See https://www.hackster.io/28377/led-light-panel-rebuild-super-dimming-and-remote-control-5478b7?team=28377
   for complete project documentation.

   Copyright 2016/2017 Rob Redford
   This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
   To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
   
*/

//#define DEBUG 2                   // define as MAXIMUM debug level, or comment out to disable debug statements

#if defined(DEBUG)
#define DEBUG_MSG(L, H, M)	       if ((L) <= DEBUG) {Serial.print("DEBUG> "); Serial.print(H); Serial.print(": "); Serial.println(M);}
#else
#define DEBUG_MSG(...)            ;
#endif

#include "common.h"

extern "C" {
   /*
     uses the ESP8266 SDK, which is in C, so we need this extern to prevent gcc from mapping the function names to C++ namespace and thus
     preventing the link editor from finding these API functions
   */
   #include <user_interface.h>
}


#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <WiFiManager.h>                     // [Local Modifications] tapzu Wifi Manager RD7 fork: https://github.com/Rom3oDelta7/WiFiManager
#include <ArduinoOTA.h>	
#include <EEPROM.h>
#include "FS.h"
#include <LEDManager.h>                     //  download from https://github.com/Rom3oDelta7/LEDManager

// globals exported to main sketch
bool 							panelEnable = true;
bool 							LEDTestSave = false;
bool                    OTAenabled = false;
bool                    WiFiConfigInProgress = false;

// main sketch externs
extern float				stops;
extern float 				battery_volts;
volatile extern uint8_t PWM_index;	
extern bool             LEDSetupOn;
extern LEDColor 			battery_color;
extern RGBLED           led;
volatile extern bool    configAborted;

extern void upButtonISR(void);
extern void downButtonISR(void);
extern void fatalError (LEDColor color);
extern void savePWMIndex(void);


// inline variable substitution
#define BRT_VAR					"%BRT%"			// PWM_index
#define DIM_VAR					"%DIM%"			// stops
#define BAT_VAR					"%BAT%"			// battery_volts
#define STATE_VAR					"%STATE%"		// output panel on/off


#define BODY_FILE					"/body.html"				// body HTML file (SPIFFS) name (static content)
#define CSS_FILE					"/css.html"					// CSS style sheet section (also static)
#define STRING_MAX				2200                    // max String obj length for holding HTML file content


/*
 user actions in HTML request stream
 also includes URI requests that invoke no action but will send the HTML file anyways (always need to reply to client)
*/
#define ACTION_UP					"UP_BTN="					// up button
#define ACTION_DN					"DN_BTN="					// down button
#define ACTION_ENABLE			"ENABLE_BTN="				// panel on/off button
#define ACTION_BATTERY			"STATUS_BTN="				// battery status LED button
#define ACTION_REFRESH			"REFRESH_BTN="				// refresh button
#define ACTION_FORGET         "FORGET_BTN="           // clear saved user credentials
#define ACTION_BRT_ENTER		"brtvalue="					// manual brightness entered
#define ACTION_DIM_ENTER		"dimvalue="					// manual f-stop value entered
typedef enum { NULL_ACTION, IGNORE, UP, DOWN, ENABLE, BATTERY, FORGET, SET_BRIGHT, SET_FSTOP } T_Action;

#define ACTION_TABLE_SIZE		11
const struct {	
   char 		action[20];
   T_Action	type;
} actionTable[ACTION_TABLE_SIZE] = {
   {ACTION_UP, 			UP},
   {ACTION_DN, 			DOWN},
   {ACTION_ENABLE, 		ENABLE},
   {ACTION_BATTERY, 		BATTERY},
   {ACTION_REFRESH, 		NULL_ACTION},
   {ACTION_FORGET,      FORGET},
   {ACTION_BRT_ENTER,	SET_BRIGHT},
   {ACTION_DIM_ENTER,	SET_FSTOP},
   {"GET / ",				NULL_ACTION},					// null actions must be at the end so they don't intercept ones above
   {"GET /index.html",	NULL_ACTION},
   {"GET /favicon.ico",	NULL_ACTION}							// always comes after another request, so skip it
};


/*
 CSS color substitution variables and color defines
*/
#define PANEL_CSS					"%PN_CSS%"
#define BAT_CSS					"%BAT_CSS%"
#define LED_CSS					"%LED_CSS%"

// CSS button background colors
#define CSS_GREEN					"greenbkgd"
#define CSS_RED					"redbkgd"
#define CSS_ORANGE				"orangebkgd"
#define CSS_GREY					"greybkgd"

WiFiServer	server(80);						// web server instance
String		bodyFile;						// String copy of body file segment
String		cssFile;							// String copy of CSS file segment	
WiFiClient	client; 							// client stream

WiFiManager wifiManager;               // IP address establishment

/*
 Create and return a unique WiFi SSID using the ESP8266 WiFi MAC address
 Form the SSID as an IP address so the user knows what address to connect to when in AP mode just in case
 (Even though the config page comes up automatically without the user having to use a browser)
*/
String createUniqueSSID (void) {
   uint8_t  mac[WL_MAC_ADDR_LENGTH];
   String   uSSID;

   WiFi.softAPmacAddress(mac);
   uSSID = String("LED ") + String ("10.") + String(mac[WL_MAC_ADDR_LENGTH - 3]) + String(".") + String(mac[WL_MAC_ADDR_LENGTH - 2]) + String(".") + String(mac[WL_MAC_ADDR_LENGTH - 1]);
   DEBUG_MSG(2, F("Derived SSID"), uSSID);
   return uSSID;
}

/*
 Create a unique class A private IP address using the ESP8266 WiFi MAC address
*/
IPAddress createUniqueIP (void) {
   uint8_t   mac[WL_MAC_ADDR_LENGTH];
   IPAddress result;

   WiFi.softAPmacAddress(mac);
   result[0] = 10;
   result[1] = mac[WL_MAC_ADDR_LENGTH - 3];
   result[2] = mac[WL_MAC_ADDR_LENGTH - 2];
   result[3] = mac[WL_MAC_ADDR_LENGTH - 1];
   DEBUG_MSG(2, F("Derived AP IP"), result);
   return result;
}

/*
 Create a unique password, again based on MAC address
 Must be 8 characters in length for WiFiManager to accept it
*/
String createUniquePassword (void) {
   uint8_t  mac[WL_MAC_ADDR_LENGTH];
   char     password[10];

   WiFi.softAPmacAddress(mac);
   sprintf(password, "LED%02x%02x%02x", mac[WL_MAC_ADDR_LENGTH - 3], mac[WL_MAC_ADDR_LENGTH - 2], mac[WL_MAC_ADDR_LENGTH - 1]);
   DEBUG_MSG(2, F("Password"), password);
   return String(password);
}

/*
 this disables the use of the SSID to broadcast the IP address in station+AP mode and sets STA mode
 also enables over-the-air firmware updates
 */
void STAMode (void) {
   if (WiFi.getMode() == WIFI_AP_STA) {
      // switch to STA mode
      DEBUG_MSG(2, F("STA+IP ==> STA mode"), "");
      WiFi.mode(WIFI_STA);
      WiFi.begin();	                      // Ref: https://github.com/esp8266/Arduino/issues/2352

      if (!OTAenabled) {
         /*
          set up OTA now that we are in STA mode
         */

         OTAenabled = true;
         // Port defaults to 8266
         ArduinoOTA.setPort(8266);

         // Hostname defaults to esp8266-[ChipID]
         //ArduinoOTA.setHostname("myesp8266");

         // No authentication by default
         // ArduinoOTA.setPassword((const char *)"123");

         ArduinoOTA.onStart([]() {
            Serial.println(F("OTA Start"));
         });
         ArduinoOTA.onEnd([]() {
            Serial.println(F("\nOTA End. Restarting ..."));
            delay(5000);
            ESP.restart();
         });
         ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
         });
         ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("OTA Error[%u]: ", error);
            switch (error) {
            case OTA_AUTH_ERROR:
               Serial.println(F("OTA Auth Failed"));
               break;

            case OTA_BEGIN_ERROR:
               Serial.println(F("OTA Begin Failed"));
               break;

            case OTA_CONNECT_ERROR:
               Serial.println(F("OTA Connect Failed"));
               break;

            case OTA_RECEIVE_ERROR:
               Serial.println(F("OTA Receive Failed"));
               break;

            case OTA_END_ERROR:
               Serial.println(F("OTA End Failed"));
               break;

            default:
               Serial.println(F("OTA Unknown error"));
               break;
            }
         });

         ArduinoOTA.begin();
         Serial.println(F("OTA Ready:"));
         Serial.print(F("\tIP address: "));
         Serial.println(WiFi.localIP());
         Serial.print(F("\tChip ID: "));
         Serial.println(ESP.getChipId(), HEX);
      }
   }
}

/*
  initialization
*/
void setupWiFi ( void ) {
   IPAddress   my_IPAddress = createUniqueIP();                 // unique IP address for AP mode to prevent conflicts with multiple devices
   String      my_password = createUniquePassword();

   // first check that there are WiFi networks to possibly connect to
   int netCount = WiFi.scanNetworks();
   bool connectToAP = false;

   if ( netCount > 0 ) {
      // try to connect (saved credentials or manual entry if not) and default to AP mode if this fails
      #ifdef DEBUG
         wifiManager.setDebugOutput(true);
      #else
         wifiManager.setDebugOutput(false);
      #endif

      DEBUG_MSG(2, F("Network scan"), netCount);

      wifiManager.setBreakAfterConfig(true);	                       // undocumented function to return if config unsuccessful
      wifiManager.setSaveCredentialsInEEPROM(true, CRED_ADDR);      // [Local mod] forces credentials to be saved in EEPROM also
      wifiManager.setExitButtonLabel("Standalone Mode");            // [Local mod] sets the label on the exit button to clarify the meaning of exiting from the portal

      //wifiManager.setAPStaticIPConfig(my_IPAddress, my_IPAddress, IPAddress(255, 0, 0, 0));    // use native WiFi class call below instead
      WiFi.softAPConfig(my_IPAddress, my_IPAddress, IPAddress(255, 0, 0, 0));	                   // workaround for callout issue - see above
      WiFi.setAutoConnect(true);
      WiFiConfigInProgress = true;

      led.setColor(LEDColor::RED, LEDColor::GREEN);                 // indicate setup mode
      led.setState(LEDState::ALTERNATE, 250UL);
      LEDSetupOn = true;                                            // set this whenever we illuminate the LED in case it was turned off by accident
      if ( wifiManager.autoConnect(createUniqueSSID().c_str(), my_password.c_str()) ) {
         // we are connected as a client and the ESP is in STA mode Ref: https://github.com/esp8266/Arduino/issues/2352
         DEBUG_MSG(2, F("Connected (STA; local WiFi)"), WiFi.localIP().toString());
         led.setColor(LEDColor::BLUE);                   // indicates STA mode
         led.setState(LEDState::ON);
         LEDSetupOn = true;
         WiFiConfigInProgress = false;

         /*

         We also need to know the address of the client as it will be running our HTTP server
         The trick is to switch to mixed mode and broadcast the local IP address in the AP SSID name.
         The user can thus find the local address by looking at the scanned SSIDs on their device

         Keep this on until the status is disabled (pressing both up & down simultaneously (or the HTML "Forget ID" button)

         */

         WiFi.mode(WIFI_AP_STA);
         
         String ssid = String("LED: ") + WiFi.localIP().toString();
         DEBUG_MSG(2, F("Local IP as SSID"), ssid);
         WiFi.softAP(ssid.c_str(), "z^VndDhK$f1!1");                // add a password just so user does not try and connect to this - this password is never used
         WiFi.softAPConfig(my_IPAddress, my_IPAddress, IPAddress(255, 0, 0, 0));

         //WiFi.reconnect();                                        // supposedly required, but does not work if this is called

         #if defined(DEBUG) && DEBUG >= 4
            Serial.println("AP_STA Diag:");
            WiFi.printDiag(Serial);
         #endif
         
         #if defined(DEBUG) && DEBUG >= 2
            WiFi.printDiag(Serial);
         #endif
      } else {
         /*
          we get here if the credentials on the setup page are incorrect, blank, or the "Exit" button was used
         */
         DEBUG_MSG(1, F("Did not connect to local WiFi"), "");
         WiFiConfigInProgress = false;
         connectToAP = true;
      }
   } else {
      DEBUG_MSG(1, F("No WiFI networks"), "");
      WiFiConfigInProgress = false;
      connectToAP = true;
   }

   // if manualy aborted, then using "manual mode" => turn off the radio to save power
   if ( configAborted ) {
      WiFi.mode(WIFI_OFF);
      led.setColor(LEDColor::CYAN);
      led.setState(LEDState::ON);
      delay(2000);                                          // display briefly to confirm mode change to user
   } else {
      if ( connectToAP ) {
         // use AP mode	 - WiFiManager leaves the ESP in AP+STA mode at this point
         DEBUG_MSG(2, F("Using AP Mode"), my_IPAddress.toString());
         led.setColor(LEDColor::PURPLE);                   // indicates AP mode
         led.setState(LEDState::ON);
         LEDSetupOn = true;

         WiFi.softAP(createUniqueSSID().c_str(), my_password.c_str());
         WiFi.softAPConfig(my_IPAddress, my_IPAddress, IPAddress(255, 0, 0, 0));
         WiFi.mode(WIFI_AP);

         #if defined(DEBUG) && DEBUG >= 3
            Serial.println(F("AP Diag:"));
            WiFi.printDiag(Serial);
         #endif
      }

      // start the server & init the SPIFFS file system
      server.begin();
      if ( !SPIFFS.begin() ) {
         #ifdef DEBUG
            Serial.println(F("Cannot open SPIFFS file system."));
         #endif
         fatalError(LEDColor::RED);
      }

      #if defined(DEBUG) && DEBUG >= 2
         Dir dir = SPIFFS.openDir("/");
         Serial.println(F("SPIFFS directory contents:"));
         while ( dir.next() ) {
            Serial.print(dir.fileName());
            File f = dir.openFile("r");
            Serial.print(": Size: ");
            Serial.println(f.size());
         }
      #endif	

      // open the BODY HTML file on the on-board FS and read it into a String (note that this string is never modified)
      File serverFile = SPIFFS.open(BODY_FILE, "r");
      bodyFile.reserve(STRING_MAX);
      if ( serverFile ) {
         if ( serverFile.available() ) {
            bodyFile = serverFile.readString();
         }
         serverFile.close();;
      } else {
         #ifdef DEBUG
            Serial.println(F("error opening BODY file"));
         #endif
         fatalError(LEDColor::RED);
      }

      // open the CSS file on the on-board FS and read it into a String (note that this string is never modified)
      serverFile = SPIFFS.open(CSS_FILE, "r");
      cssFile.reserve(STRING_MAX);
      if ( serverFile ) {
         if ( serverFile.available() ) {
            cssFile = serverFile.readString();
         }
         serverFile.close();;
      } else {
         #ifdef DEBUG
            Serial.println(F("error opening CSS file"));
         #endif
         fatalError(LEDColor::RED);
      }
   }
}


/*
  send body string as formatted HTML to client
  Because of issues in the String class, we need to output complete HTML file in segments (header, CSS, working HTML code)
*/
void sendHTML (const int code, const char *content_type, const String &body) {
   String title;

   // get correct IP to add to page title
   if ( WiFi.getMode() == WIFI_AP ) {
      title = String("LED Panel ") + WiFi.softAPIP().toString();
   } else {
      title = String("LED Panel ") + WiFi.localIP().toString();
   }
   DEBUG_MSG(2, F("constructed title for web page"), title);

   // header
   client.println(String("HTTP/1.1 ") + String(code) + String(" OK"));
   client.println(String("Content-Type: ") + String(content_type));
	 int content_len = cssFile.length() + body.length();
	 client.println(String("Content-Length: ") + String(content_len));
   client.println("Connection: close\n");
   client.println(String("<!DOCTYPE HTML> <HTML> <HEAD> <TITLE>") + title + String("</TITLE> </HEAD>"));

   // send CSS file contents
   client.println(cssFile);

   // send body
   client.println(body);
   client.println("</HTML>");
}

/*
 clear saved WiFi credentials
 */
void clearCredentials(void) {
   if (EEPROM.read(CRED_ADDR) == EEPROM_KEY) {
      // EEPROM backup saved credentials
      for (int i = 1; i <= sizeof(struct station_config); i++) {
         EEPROM.write(CRED_ADDR + i, 0);
      }
      EEPROM.commit();
   }
   /*
    delete credentials from saved parameter area (last 16KB of flash) using direct ESP SDK library calls
    while we could call WiFi.disconnect(), it will clear the credentials as well as disconnecting from the AP,
    which is undesirable in this case
    */
   struct station_config conf;
   wifi_station_get_config(&conf);
   *conf.ssid = '\0';
   *conf.password = '\0';

   ETS_UART_INTR_DISABLE();
   wifi_station_set_config(&conf);
   ETS_UART_INTR_ENABLE();
}

/*
 depending on what the requested action is, make the appropriate changes to the HTML code stream
 (e.g. variable substation) and state changes to the main sketch code (e.g. increasing brightness) and send the modified HTML
 stream to the client
*/
void sendResponse ( const T_Action actionType, const String &url ) {
   String indexModified = bodyFile;								// all changes made to this String

   DEBUG_MSG(1, F("ACTION"), actionType);

   #if defined(DEBUG) && DEBUG >= 4
      Serial.println("\nIndex file, unmodified:");
      Serial.print(indexModified);
   #endif
   
   /*
    process user action
    When changing the brightness level, always recalulate the f-stop value so the WiFi interface values stay consistent
    Otherwise, it will not be updated until proceed by panelSerevice()
   */
   indexModified.reserve(STRING_MAX);
   if ( actionType != IGNORE ) {
      uint8_t  idx;

      switch ( actionType ) {
      case UP:
         // up button pressed - increment brightness index
         PWM_index = constrain(PWM_index+1, 0, PWM_index_max-1);
         stops = float((PWM_index_max -1) - PWM_index) / -2.0;
         break;
         
      case DOWN:
         // down button pressed
         PWM_index = constrain(PWM_index-1, 0, PWM_index_max-1);
         stops = float((PWM_index_max -1) - PWM_index) / -2.0;
         break;

         
      case SET_BRIGHT:
         // user-entered brightness value
         idx = url.lastIndexOf('=');
         if ( idx ) {
            String value = url.substring(idx+1);
            PWM_index = constrain(value.toInt(), 0, PWM_index_max-1);
            stops = float((PWM_index_max -1) - PWM_index) / -2.0;
         }
         break;
      
      case SET_FSTOP:
         // user-entered f-stop value (always negative): convert back to index
         idx = url.lastIndexOf('-');
         if ( idx ) {
            String value = url.substring(idx+1);
            PWM_index = constrain((int)((PWM_index_max-1) - (value.toFloat() * 2.0)), 0, PWM_index_max-1);
            stops = float((PWM_index_max -1) - PWM_index) / -2.0;
         }
         break;
         
      case ENABLE:
         // toggle panel output enable
         panelEnable = !panelEnable;
         break;
         
      case BATTERY:
         // toggle LED status indicator on panel and save current brightness value to EEPROM
         if ( LEDTestSave ) {
            LEDTestSave = false;
         } else {
            LEDTestSave = true;
            savePWMIndex();
         }
         break;	

      case FORGET:
         // clear saved WiFi credentials
         clearCredentials();
         break;
         
      case NULL_ACTION:
				break;
				
      default:
         break;
      }
   
      DEBUG_MSG(1, F("BRIGHTNESS"), PWM_index);
      DEBUG_MSG(1, F("F-STOPS"), stops);
      DEBUG_MSG(1, F("PANEL ENABLE"), panelEnable);
      DEBUG_MSG(1, F("LED TEST/SAVE"), LEDTestSave);

   
      // change color placeholders to corresponding CSS to match current state
      if ( panelEnable ) {
         // panel output enab;le
         indexModified.replace(String(PANEL_CSS), String(CSS_GREEN));
      } else {
         indexModified.replace(String(PANEL_CSS), String(CSS_RED));
      }
      if ( LEDTestSave ) {
         // Test/save button (enabled physical LED on the unit)
         indexModified.replace(String(BAT_CSS), String(CSS_GREEN));
      } else {
         indexModified.replace(String(BAT_CSS), String(CSS_RED));
      }
      // match the battery value <label> field color with the LED state color
      switch ( battery_color ) {
      case LEDColor::GREEN:
         indexModified.replace(String(LED_CSS), String("green"));
         break;
      
      case LEDColor::ORANGE:
         indexModified.replace(String(LED_CSS), String("orange"));
         break;
         
      case LEDColor::RED:
         indexModified.replace(String(LED_CSS), String("red"));
         break;
         
      default:
         indexModified.replace(String(LED_CSS), String("white"));
         break;
         
      }
      // substitute current data variables for the placeholders in the base HTML file
      indexModified.replace(String(BRT_VAR), String(PWM_index));
      indexModified.replace(String(DIM_VAR), String(stops, 2));
      indexModified.replace(String(BAT_VAR), String(battery_volts, 2));
      indexModified.replace(String(STATE_VAR), panelEnable ? String("On") : String("Off"));

      #if defined(DEBUG) && DEBUG >= 4
         Serial.println(F("\BODY FILE **MODIFIED**"));
         Serial.print(indexModified);
      #endif
      // finally, send to the client
      sendHTML(200, "text/html", indexModified);
   }
}

/*
 Read a line of text (ending in \r) from a connected client (note: \r is not returned in the String)
 The ESP8266 example uses readStringUntil, but it seems that *all* readString Stream functions
 are e x c r u c i a t i n g l y slow, so do it with read() instead
 */
String readLineFromClient ( WiFiClient& client, const int bufSize = 80 ) {
   bool EOL = false;
   
   char* buffer = new char[bufSize];
   char* p = buffer;
   do {
      byte ch = client.read();
      //Serial.printf("\tREAD: %c (%02x)", ch, ch);
      if ( (ch == 0xFF) || (ch == '\r') ) {
         EOL = true;
      } else {
         *p++ = static_cast<char>(ch);
      }
   } while ( !EOL );
   *p = '\0';
   String result = String(buffer);
   delete buffer;
   return result;
}

/*
  main WiFi service routine
*/
void WiFiService ( void ) {
   bool responseSent = false;
   
   //Serial.printf("Heap: %d\n", ESP.getFreeHeap()); 
   delay(100);                                      // not sure why this is needed here, but performance is very  sluggish without it
   client = server.available();
   if ( client && client.connected() ) {
      DEBUG_MSG(0, F("Client connected"), "");
      if ( LEDSetupOn ) {
         // we can disable this once the user has established a connection
         LEDSetupOn = false;
         STAMode();
      }

      // retrieve the URI request from the client stream
      String uri = readLineFromClient(client);

      DEBUG_MSG(0, F("Client URI"), uri);

      client.flush();
      
      // scan the action table to get the action and send appropriate response
      for ( uint8_t i = 0; i < ACTION_TABLE_SIZE; i++ ) {
         if ( uri.indexOf(actionTable[i].action) != -1 ) {
            // done once we found the first match (this is why null actions are the the bottom of the table)
            sendResponse(actionTable[i].type, uri);
            responseSent = true;
            break;
         }
      }
      if ( !responseSent ) {
         // keep the connection alive
         sendResponse(NULL_ACTION, uri);
      }
      client.stop();
   }
}

